package com.bootcamp;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.bootcamp.YachtCategory.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class YachtTest {

    @Test
    public void expectScoreToBe50WhenAllOccurrencesSameAndYachtCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(5, 5, 5, 5, 5);
        Yacht yacht = new Yacht(diceOutcomeList, YACHT);

        assertEquals(50, yacht.score());
    }

    @Test
    public void expectScoreToBe0WhenAllOccurrencesNotSameAndYachtCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 5, 5, 5, 5);
        Yacht yacht = new Yacht(diceOutcomeList, YACHT);

        assertEquals(0, yacht.score());
    }

    @Test
    public void expectScoreToBe0When1AppearTwiceAnd2AppearThriceAndYachtCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 1, 2, 2, 2);
        Yacht yacht = new Yacht(diceOutcomeList, YACHT);

        assertEquals(0, yacht.score());
    }

    @Test
    public void expectScoreToBe10when2AppearInAllDicesAndChoiceCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(2, 2, 2, 2, 2);
        Yacht yacht = new Yacht(diceOutcomeList, CHOICE);

        assertEquals(10, yacht.score());
    }

    @Test
    public void expectScoreToBe13when3AppearThriceAnd2AppearTwiceAndInAllDicesAndChoiceCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(2, 3, 3, 3, 2);
        Yacht yacht = new Yacht(diceOutcomeList, CHOICE);

        assertEquals(13, yacht.score());
    }

    @Test
    public void expectScore30WhenOccurrencesOnDice2_3_4_5_6() {
        List<Integer> diceOutcomeList = Arrays.asList(2, 3, 4, 5, 6);
        Yacht yacht = new Yacht(diceOutcomeList, BIG_STRAIGHT);

        assertEquals(30, yacht.score());
    }

    @Test
    public void expectScore0WhenOccurrencesOnDice1_3_4_5_6() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 3, 4, 5, 6);
        Yacht yacht = new Yacht(diceOutcomeList, BIG_STRAIGHT);

        assertEquals(0, yacht.score());
    }

    @Test
    public void expectScore30WhenOccurrencesOnDice1_2_3_4_5AndLittleStraightCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 2, 3, 4, 5);
        Yacht yacht = new Yacht(diceOutcomeList, LITTLE_STRAIGHT);

        assertEquals(30, yacht.score());
    }

    @Test
    public void expectScore30WhenOccurrencesOnDice1_2_4_4_5AndLittleStraightCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 2, 4, 4, 5);
        Yacht yacht = new Yacht(diceOutcomeList, LITTLE_STRAIGHT);

        assertEquals(0, yacht.score());
    }

    @Test
    public void expectScore16When4AppearsForFiveTimesAndFourOfAKindCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 4, 4, 4);
        Yacht yacht = new Yacht(diceOutcomeList, FOUR_OF_A_KIND);

        assertEquals(16, yacht.score());
    }


    @Test
    public void expectScore16When4AppearsForThreeTimesAnd2AppearsForTwoTimesAndFullHouseCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 4, 2, 2);
        Yacht yacht = new Yacht(diceOutcomeList, FULL_HOUSE);

        assertEquals(16, yacht.score());
    }

    @Test
    public void expectScore0When4AppearsForTwoTimesAnd2AppearsForTwoTimesAndFullHouseCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 3, 2, 2);
        Yacht yacht = new Yacht(diceOutcomeList, FULL_HOUSE);

        assertEquals(0, yacht.score());
    }

    @Test
    public void expectScore24When6AppearsForFourTimesAndSixesCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(6, 6, 6, 6, 1);
        Yacht yacht = new Yacht(diceOutcomeList, SIXES);

        assertEquals(24, yacht.score());
    }

    @Test
    public void expectScore15When5AppearsForThreeTimesAndFivesCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(6, 6, 5, 5, 5);
        Yacht yacht = new Yacht(diceOutcomeList, FIVES);

        assertEquals(15, yacht.score());
    }

    @Test
    public void expectScore8When4AppearsForTWOTimesAndFourCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 5, 5, 5);
        Yacht yacht = new Yacht(diceOutcomeList, FOURS);

        assertEquals(8, yacht.score());
    }

    @Test
    public void expectScore3When3AppearsForThreeTimesAndThreeCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 3, 3, 3);
        Yacht yacht = new Yacht(diceOutcomeList, THREE);

        assertEquals(9, yacht.score());
    }

    @Test
    public void expectScore8When2AppearsForFourTimesAndTwoCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 2, 2, 2, 2);
        Yacht yacht = new Yacht(diceOutcomeList, TWO);

        assertEquals(8, yacht.score());
    }

    @Test
    public void expectScore5When1AppearsForFiveTimesAndOneCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 1, 1, 1, 1);
        Yacht yacht = new Yacht(diceOutcomeList, ONE);

        assertEquals(5, yacht.score());
    }
}
