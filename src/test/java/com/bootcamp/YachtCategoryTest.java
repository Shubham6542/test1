package com.bootcamp;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.bootcamp.YachtCategory.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class YachtCategoryTest {
    @Test
    public void expectScoreToBe50WhenAllOccurrencesSameAndYachtCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(5, 5, 5, 5, 5);
        assertEquals(50, YACHT.score(diceOutcomeList));
    }

    @Test
    public void expectScoreToBe0WhenAllOccurrencesNotSameAndYachtCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 5, 5, 5, 5);
        assertEquals(0, YACHT.score(diceOutcomeList));
    }

    @Test
    public void expectScoreToBe10when2AppearInAllDicesAndChoiceCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(2, 2, 2, 2, 2);
        assertEquals(10, CHOICE.score(diceOutcomeList));
    }

    @Test
    public void expectScoreToBe14when2AppearFourTimesAnd6AppearOneTimesAndChoiceCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(2, 6, 2, 2, 2);
        assertEquals(14, CHOICE.score(diceOutcomeList));
    }

    @Test
    public void expectScore30WhenOccurrencesOnDice1_2_3_4_5AndLittleStraightCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 2, 3, 4, 5);
        assertEquals(30, LITTLE_STRAIGHT.score(diceOutcomeList));
    }

    @Test
    public void expectScore0WhenOccurrencesOnDice1_3_4_5_6() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 3, 4, 5, 6);
        assertEquals(0, BIG_STRAIGHT.score(diceOutcomeList));
    }

    @Test
    public void expectScore30WhenOccurrencesOnDice2_3_4_5_6() {
        List<Integer> diceOutcomeList = Arrays.asList(2, 3, 4, 5, 6);
        assertEquals(30, BIG_STRAIGHT.score(diceOutcomeList));
    }

    @Test
    public void expectScore24WhenFourSixAppearsAndFourOfAKindCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(6, 6, 4, 6, 6);
        assertEquals(24, FOUR_OF_A_KIND.score(diceOutcomeList));
    }

    @Test
    public void expectScore16When4AppearsForFiveTimesAndFourOfAKindCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 4, 4, 4);
        assertEquals(16, FOUR_OF_A_KIND.score(diceOutcomeList));
    }

    @Test
    public void expectScore16When4AppearsForThreeTimesAnd2AppearsForTwoTimesAndFullHouseCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 4, 2, 2);
        assertEquals(16, FULL_HOUSE.score(diceOutcomeList));
    }

    @Test
    public void expectScore0When4AppearsForTwoTimesAnd2AppearsForTwoTimesAndFullHouseCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 3, 2, 2);
        assertEquals(0, FULL_HOUSE.score(diceOutcomeList));
    }

    @Test
    public void expectScore24When6AppearsForFourTimesAndSixesCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(6, 6, 6, 6, 1);
        assertEquals(24, SIXES.score(diceOutcomeList));
    }

    @Test
    public void expectScore15When5AppearsForThreeTimesAndFivesCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(6, 6, 5, 5, 5);
        assertEquals(15, FIVES.score(diceOutcomeList));
    }

    @Test
    public void expectScore8When4AppearsForTWOTimesAndFourCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 5, 5, 5);
        assertEquals(8, FOURS.score(diceOutcomeList));
    }

    @Test
    public void expectScore3When3AppearsForThreeTimesAndThreeCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 4, 3, 3, 3);
        assertEquals(9, THREE.score(diceOutcomeList));
    }

    @Test
    public void expectScore8When2AppearsForFourTimesAndTwoCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(4, 2, 2, 2, 2);
        assertEquals(8, TWO.score(diceOutcomeList));
    }

    @Test
    public void expectScore5When1AppearsForFiveTimesAndOneCategory() {
        List<Integer> diceOutcomeList = Arrays.asList(1, 1, 1, 1, 1);
        assertEquals(5, ONE.score(diceOutcomeList));
    }


}
