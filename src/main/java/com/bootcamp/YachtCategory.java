package com.bootcamp;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

//Represents different types of yacht.
enum YachtCategory {
    YACHT {
        int score(List<Integer> diceOutcomeList) {
            if (Collections.frequency(diceOutcomeList, diceOutcomeList.get(0)) == 5) {
                return 50;
            }
            return 0;
        }
    },

    CHOICE {
        int score(List<Integer> diceOutcomeList) {
            return totalSum(diceOutcomeList);
        }
    },

    BIG_STRAIGHT {
        int score(List<Integer> diceOutcomeList) {
            int BIG_STRAIGHT_SUM = 20;
            if (totalSum(diceOutcomeList) == BIG_STRAIGHT_SUM) {
                return 30;
            }
            return 0;
        }
    },
    LITTLE_STRAIGHT {
        int score(List<Integer> diceOutcomeList) {
            int LITTLE_STRAIGHT_SUM = 15;
            if (totalSum(diceOutcomeList) == LITTLE_STRAIGHT_SUM) {
                return 30;
            }
            return 0;
        }
    },
    FOUR_OF_A_KIND {
        int score(List<Integer> diceOutcomeList) {
            Map<Integer, Long> frequencyMap = getFrequencyMap(diceOutcomeList);
            final int[] output = {0};

            frequencyMap.forEach((k, v) -> {
                if (v >= 4) {
                    output[0] = k * 4;
                }
            });
            return output[0];
        }
    },
    FULL_HOUSE {
        int score(List<Integer> diceOutcomeList) {
            Map<Integer, Long> frequencyMap = getFrequencyMap(diceOutcomeList);
            final int[] output = {0};
            matchFullHouseConstraint(diceOutcomeList, frequencyMap, output);
            return output[0];
        }

        private void matchFullHouseConstraint(List<Integer> diceOutcomeList, Map<Integer, Long> frequencyMap, int[] output) {
            frequencyMap.forEach((k, v) -> {
                if (v == 3) {
                    frequencyMap.forEach((k1, v1) -> {
                        if (v1 == 2) {
                            output[0] = totalSum(diceOutcomeList);
                        }
                    });
                }
            });
        }
    },
    SIXES(6),
    FIVES(5),
    FOURS(4),
    THREE(3),
    TWO(2),
    ONE(1);

    private static Map<Integer, Long> getFrequencyMap(List<Integer> diceOutcomeList) {
        return diceOutcomeList.stream()
                .collect(Collectors.groupingBy(Function.identity(),
                        Collectors.counting()));
    }

    private final int value;

    YachtCategory() {
        this(0);
    }

    YachtCategory(int value) {
        this.value = value;
    }

    int score(List<Integer> diceOutcomeList) {
        Map<Integer, Long> frequencyMap = getFrequencyMap(diceOutcomeList);
        return (int) (value * frequencyMap.get(value));
    }

    protected int totalSum(List<Integer> diceOutcomeList) {
        return diceOutcomeList.stream()
                .mapToInt(Integer::intValue)
                .sum();

    }
}
