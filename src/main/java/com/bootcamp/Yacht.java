package com.bootcamp;

import java.util.List;

class Yacht {

    List<Integer> diceOutcomeList;
    YachtCategory yacht;

    Yacht(List<Integer> diceOutcomeList, YachtCategory yacht) {
        this.diceOutcomeList = diceOutcomeList;
        this.yacht = yacht;
    }

    int score() {
        return yacht.score(diceOutcomeList);
    }
}
